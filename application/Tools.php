<?php
namespace app\http;

use think\swoole\Server;
use think\Db;
class Tools extends Server
{
    protected $host = '0.0.0.0';
    protected $port = 9501;
    protected $serverType = 'socket';
    protected $option = [
        'worker_num' => 4, // 设置启动的Worker进程数
        'daemonize' => false, //守护进程化。
        'backlog' => 128, //Listen队列长度，
        'dispatch_mode' => 2,
        'heartbeat_idle_time' => 130,
        'heartbeat_check_interval' => 60,
        /*'type'         => 'socket', // 服务类型 支持 socket http server
        'mode'         => SWOOLE_PROCESS,
        'socket_type'  => SWOOLE_SOCK_TCP,*/
    ];

    /*public function onReceive($server, $fd, $from_id, $data)
    {
        var_dump($fd);
        $server->send($fd, 'Swoole1: '.$data);
    }
    public function onRequest($request, $response) {
        $this->onOpen($request, $response);
        //$response->end("<h1>Hello Swoole1. #" . rand(1000, 9999) . "</h1>");
    }*/

    public function onOpen($server, $request)
    {

        $res = Db::table('chat_group')->find(['g_id'=>2]);
        var_dump($request);
    }

    public function onMessage($server, $request)
    {

    }

    public function onClose($server, $fd)
    {

    }
}