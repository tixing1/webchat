<?php
namespace app\http;

use think\swoole\Server;
use think\Db;
class Swoole extends Server
{
    protected $redis;
    protected $host = '0.0.0.0';
    protected $port = 9501;
    protected $serverType = 'socket';
    protected $option = [
        'worker_num' => 4, // 设置启动的Worker进程数
        'daemonize' => false, //守护进程化。
        'backlog' => 128, //Listen队列长度，
        'dispatch_mode' => 2,
        'heartbeat_idle_time' => 130,
        'heartbeat_check_interval' => 60,
    ];

    public function __construct() {
        //服务启动时候清除所有用户的 fd 描述符
        Db::table('chat_user')->where('uid','>',0)->update(['u_fd'=>'']);
        parent::__construct();
    }

    public function onWorkerStart($server, $worker_id)
    {
        //设置定时器,每20秒获取一次系统提示消息
        if($worker_id == 0) {
            swoole_timer_tick(20000, function ($timer_id) use ($server) {
                $result = Db::table('chat_sys_msg')->alias('s')->field('count(s.s_id) as count,s.s_to_id, u.u_fd')->join('chat_user u', 'u.uid=s.s_to_id')->where('s_status', 0)->group('s_to_id')->select();
                if(!empty($result)){
                    foreach ($result as $k => $val) {
                        if ($val['u_fd']) {
                            $server->push($val['u_fd'], json_encode(['type' => 'system', 'boxmsg' => $val['count']]));
                        }
                    }
                }

            });

            //每5秒钟获取一次用户 添加的好友或群的 申请状态
            /*swoole_timer_tick(5000, function ($timer_id) use ($server) {
                    $result = Db::table('chat_checking')->alias('s')->field('count(s.s_id) as count,s.s_to_id, u.u_fd')->join('chat_user u','u.uid=s.s_to_id')->where('s_status',0)->group('s_to_id')->select();
                    foreach ($result as $k=> $val){
                        if($val['u_fd']){
                            $server->push($val['u_fd'],json_encode(['type'=>'system','boxmsg'=>$val['count']]));
                        }
                    }
                });
            });*/
        }


    }

    /*public function onRequest($request,$response)
    {
        global $server;
        $index = new Test();
        var_dump($request->get);
        var_dump($request->fd);
        //$response->end('');
    }*/
    public function onOpen($server, $request)
    {

        $fd = $request->fd;
        $uid = $request->get['uid'] ?? '';
        if(!$uid){
            $server->push($fd, json_encode(['code'=>0,'msg'=>'请先登陆']));
            $server->close($fd);
            return;
        }
        //绑定uid
        if($request->get['flg'] == 'bind'){
            Db::table('chat_user')->where('uid',$uid)->update(['u_fd'=>$fd]);
            //$this->redis->set('uid:'.$uid,$fd);
            var_dump($uid.':'.$fd."\n");
        }
    }

    public function onMessage($server, $frame)
    {
        $data = json_decode($frame->data,true);
        if($data['type'] == 'ping'){
            //心跳监测
        }else{
            //$this->redis->get('uid:'.$data['data']['to']['id']);
            $message['username'] = $data['data']['mine']['username'];
            $message['avatar'] = $data['data']['mine']['avatar'];
            $message['id'] = $data['data']['to']['type']=='friend' ? $data['data']['mine']['id'] : $data['data']['to']['id'];
            $message['type'] = $data['data']['to']['type'];
            $message['content'] = $data['data']['mine']['content'];
            $message['cid'] = 0;
            $message['mine'] = false;
            $message['fromid'] = $data['data']['mine']['id'];
            $message['timestsmp'] = time();

            switch ($data['type']){
                case 'friend': //私聊
                    $userinfo= Db::table('chat_user')->find(['uid',$data['data']['to']['id']]);
                    $server->push($userinfo['u_fd'],json_encode($message,320));
                    break;
                case 'group': //群聊
                    $g_id = $data['data']['to']['id']; //当前聊天群的id
                    $ids = Db::table('chat_group')->find(['g_id'=>$g_id]);
                    $remembers = Db::table('chat_user')->where('uid','in',$ids['g_group_id'])->where('u_fd','<>','')->select();
                    //$ids_arr = explode(',',rtrim($ids['g_group_id'],','));
                    if($remembers){
                        foreach ($remembers as $key => $value){
                            if($value['u_fd'] && $value['uid'] != $data['data']['mine']['id']){
                                $server->push($value['u_fd'],json_encode($message,320));
                            }
                        }
                    }

                    break;
                default:
                    break;
            }
        }
    }


    public function onClose($server, $fd)
    {
        Db::table('chat_user')->where('u_fd',$fd)->update(['u_fd'=>'']);
        echo "client {$fd} closed\n";
    }
}