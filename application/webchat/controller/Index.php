<?php
namespace app\webchat\controller;

use think\Controller;

use think\Db;
class Index extends Controller
{
    public function initialize()
    {
        //验证是否登录
        if(!session('chat_user')){
            $this->redirect('/chat/login');
        }
    }

    public function index()
    {

        $access_token = '';
        $uid = session('chat_user.uid');
        $this->assign('uid',$uid);
        return view('index');
    }


    //主面板初始化
    public function getInit()
    {
        
        $uid = session('chat_user.uid');
        //查询当前登录用户的信息
        $userinfo = Db::table('chat_user')->find(['uid'=>$uid]);
        $data['mine']['username'] = $userinfo['u_nick'];
        $data['mine']['id'] = $userinfo['uid'];
        $data['mine']['status'] = $userinfo['u_online'];
        $data['mine']['sign'] = $userinfo['u_sign'];
        $data['mine']['avatar'] = $userinfo['u_photo'];
        //查询好友以及好友分组

        $friends = Db::table('chat_friend')->where('f_uid',$uid)->select();

        foreach ($friends as $key => $value){
            $data['friend'][$key]['groupname'] = $value['f_type_name'];
            $data['friend'][$key]['id'] = $value['f_id'];
            if(!$value['friends_id']){
                $data['friend'][$key]['list'] = array();
                continue;
            }
            $groupF = Db::table('chat_user')->where('uid','in',$value['friends_id'])->select();
            foreach ($groupF as $k => $val){
                $data['friend'][$key]['list'][$k]['username'] = $val['u_nick'];
                $data['friend'][$key]['list'][$k]['id']       = $val['uid'];
                $data['friend'][$key]['list'][$k]['status']   = $val['u_online'];
                $data['friend'][$key]['list'][$k]['sign']     = $val['u_sign'];
                $data['friend'][$key]['list'][$k]['avatar']   = $val['u_photo'];
            }

        }

        //查询群列表
        $groups = Db::table('chat_group')->where('g_group_id','like','%'.$uid.',%')->select();
        if($groups){
            foreach ($groups as $key => $value){
                $data['group'][$key]['groupname'] = $value['g_name'];
                $data['group'][$key]['id'] = $value['g_id'];
                $data['group'][$key]['avatar'] = $value['g_photo'];
            }

        }

        $info = json_encode(['code'=>0,'msg'=>'','data'=>$data],320);
        echo $info;

    }

//查找页面：获取要添加的 好友/群组 列表
    public function findFriend()
    {
        //access_token 验证

        $uid = session('chat_user.uid');
        //查询非好友的个人列表
        $friends = Db::table('chat_friend')->field('friends_id')->where('f_uid',$uid)->select();
        $f_ids = '';
        if(!empty($friends)){

            foreach ($friends as $value){
                $f_ids .= $value['friends_id'];
            }
        }
        $f_ids .= $uid;
        $data_list = Db::table('chat_user')->where('uid','not in',$f_ids)->select();

        //查询没有加入过的群
        $group = Db::table('chat_ingroup')->field('in_group_id')->where('in_uid',$uid)->find();
        $in_group = $group ? $group : '';
        $groups = Db::table('chat_group')->where('g_uid','not in',$in_group)->select();

        $this->assign('data_list',$data_list);
        $this->assign('groups',$groups);
        return view('find');
    }
    //添加的 好友/群组 发送验证信息
    public function addFriendGroup()
    {
        //access_token 验证
        $add_type = input('add_type'); //添加类型 friend 好友， group群组
        $data['c_uid']      = session('chat_user.uid');

        $data['c_type'] = input('fenzu_id');//被加的分组id
        $data['c_msg']      = input('check_msg'); //验证信息
        $data['c_created']      = date('Y-m-d H:i:s');
        if($add_type == 'friend'){
            $data['c_add_id']   = input('id'); //被加人的id  或加入群组的id
        }else{
            $data['c_group_id']   = input('id'); //被加人的id  或加入群组的id
        }

        //添加好友
        if(isset($data['c_add_id']) && $data['c_add_id']){
            //已经是好友
            $res = Db::table('chat_friend')->where('f_uid',$data['c_uid'])->where('friends_id','like',"%{$data['c_add_id']},%")->find();
            if($res){
                return ['code'=>1,'msg'=>'你们已经是好友了！'];
            }
            //还不是好友
            //查询好友是否已经发送过添加的验证请求 1等待验证， 2验证通过已经是好友了， 3拒绝添加
            $data['c_status'] = 1;
            $info = Db::table('chat_checking')->where('c_uid',$data['c_uid'])->where('c_add_id',$data['c_add_id'])->find();
            if(!$info['c_status']){
                $res = Db::table('chat_checking')->insert($data);
            }
            if($info['c_status'] == 3 || $info['c_status']==1){//以前添加被拒绝或者还在等待验证
                $res = Db::table('chat_checking')->where('c_id',$info['c_id'])->update($data);
            }

            if(!$res){
                return ['code'=>1,'msg'=>'验证消息发送失败，请稍后重试！'];
            }
            //添加系统提示信息
            Db::table('chat_sys_msg')->insert(['s_from_id'=>session('chat_user.uid'),'s_to_id'=>input('id')]);
            return ['code'=>0,'msg'=>'验证信息已发送，请等待...'];
        }

        //加入群组
        if(isset($data['c_group_id']) && $data['c_group_id']){
            //已经加入过此群
            $result = Db::table('chat_group')->where('g_id',$data['c_group_id'])->where('g_group_id','like',"%{$data['c_uid']},%")->find();
            if($result){
                return ['code'=>1,'msg'=>'你已经是此群的成员了！'];
            }

            //还没加入此群

            $info = Db::table('chat_checking')->where('c_uid',$data['c_uid'])->where('c_group_id',$data['c_group_id'])->find();
            if(!isset($info['c_status'])){
                $result = Db::table('chat_checking')->insert($data);
            }
            //以前加群被拒绝，修改状态为未验证
            $data['c_status'] = 1;
            if($info['c_status'] == 3){
                $result = Db::table('chat_checking')->where('c_id',$info['c_id'])->update($data);
            }
            if(!$result){
                return ['code'=>1,'msg'=>'验证消息发送失败，请稍后重试！'];
            }
            return ['code'=>0,'msg'=>'验证信息已发送，请等待...'];
        }
        return ['code'=>1,'msg'=>'传递参数错误'];
    }

    //消息盒子
    public function msgBox()
    {
        $uid = session('chat_user.uid');
        //更新系统系统提醒状态为已看
        Db::table('chat_sys_msg')->where('s_to_id',$uid)->update(['s_status'=>1]);
        //当前用户所创建的所有群id
        $group_ids = Db::table('chat_group')->field('g_id')->where('g_uid',$uid)->select();
        $str_ids = array_column($group_ids,'g_id');
        //好友/群 添加时的验证信息
        $msg = Db::table('chat_checking')
            ->where('c_add_id',$uid)
            ->whereOr('c_uid',$uid)
            ->whereOr('c_group_id','in',implode(',',$str_ids))
            ->order('c_id','desc')
            ->select();

        foreach ($msg as $key => $value){
            //发送的好友申请
            if($value['c_uid'] == $uid && empty($value['c_group_id'])){
                if($value['c_status'] == 1){//等待验证
                    unset($msg[$key]);
                }else{//已经验证
                    $uname = Db::table('chat_user')->where('uid',$value['c_add_id'])->find();
                    $msg[$key]['uname'] = $uname;
                }
            }
            //接收的好友申请
            if($value['c_add_id'] == $uid){
                $uname = Db::table('chat_user')->where('uid',$value['c_uid'])->find();
                $msg[$key]['uname'] = $uname;
            }

            //发送的群申请
            if($value['c_group_id'] && $value['c_uid'] == $uid){
                if($value['c_status'] == 1){//等待验证
                    unset($msg[$key]);
                }else {//已经验证
                    $gname = Db::table('chat_group')->where('g_id', $value['c_group_id'])->find();
                    $msg[$key]['gname'] = $gname;
                }
            }

            //接收的群申请
            if(in_array($value['c_group_id'],$str_ids)){
                $uname = Db::table('chat_user')->where('uid',$value['c_uid'])->find();
                $gname = Db::table('chat_group')->where('g_id',$value['c_group_id'])->find();
                $gname['u_nick'] = $uname['u_nick'];
                $msg[$key]['gname'] = $gname;
            }

        }
        /*echo '<pre>';
        var_dump($msg);die;*/
        $this->assign('uid',$uid);
        $this->assign('str_ids',$str_ids);
        $this->assign('msg',$msg);
        $this->assign('msg1',json_encode($msg));
        return view('msgbox');
    }

    //同意好友申请
    public function agreeFriend()
    {
        $uid = session('chat_user.uid');//我的id
        $group = intval(input('group'));//我设置的分组id 或群id
        $friend_id = intval(input('uid'));//好友的id
        $friend_group = intval(input('from_group'));//好友设置的分组id
        $c_id = intval(input('c_id'));//申请信息的id
        $type = input('type');//类型  group：群  friend: 好友

        Db::startTrans();
        try {
            if($type == 'group'){
                $res = Db::table('chat_group')->where('g_id',$group)->find();
                $up_group_ids = $res['g_group_id'].$friend_id.',';
                Db::table('chat_group')->where('g_id',$group)->update(['g_group_id'=>$up_group_ids]);
                Db::table('chat_checking')->where('c_id',$c_id)->update(['c_status'=>2]);
            }else{
                $fids = Db::table('chat_friend')->where('f_id','in',"$group,$friend_group")->select();
                foreach ($fids as $key => $value){
                    if($value['f_id'] == $group){
                        //我添加好友到我的分组
                        $f_id = $group;
                        $up_friends_id  = $value['friends_id'] . $friend_id .',';
                    }else{
                        //好友添加我到他的分组
                        $f_id = $friend_group;
                        $up_friends_id  = $value['friends_id'] . $uid .',';
                    }
                    Db::table('chat_friend')->where('f_id',$f_id)->update(['friends_id'=>$up_friends_id]);
                }
                //更新申请验证信息状态 2同意加好友  3拒绝加好友
                Db::table('chat_checking')->where('c_id',$c_id)->update(['c_status'=>'2','c_created'=>date('Y-m-d H:i:s')]);
            }

            Db::table('chat_sys_msg')->insert(['s_from_id'=>$uid,'s_to_id'=>$friend_id]);
            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return ['code'=>1,'msg'=>'操作失败，请稍后重试'];
        }
        return ['code'=>0,'msg'=>'操作成功'];


    }

    //拒绝好友申请
    public function refuseFriend()
    {
        $c_id = intval(input('c_id'));
        $c_uid = intval(input('uid'));

        $res = Db::table('chat_checking')
            ->where('c_id',$c_id)
            ->where('c_uid',$c_uid)
            ->update(['c_status'=>3,'c_created'=>date('Y-m-d H:i:s')]);
        if($res){
            Db::table('chat_sys_msg')->insert(['s_from_id'=>session('chat_user.uid'), 's_to_id'=>$c_uid]);
            return ['code'=>0,'msg'=>'操作成功'];
        }
        return ['code'=>1,'msg'=>'操作失败，请稍后重试'];
    }

    //创建群
    public function createGroup()
    {
        $data['g_name'] = input('gname');
        if(!$data['g_name']){
            return ['code'=>1,'msg'=>'参数错误，请稍后重新操作'];
        }
        $data['g_uid'] = session('chat_user.uid');
        $data['g_photo'] = 'public/upload/images/qun.jpg';
        $result = Db::table('chat_group')->insertGetId($data);
        if(!$result){
            return ['code'=>1,'msg'=>'创建失败，请稍后重新操作'];
        }
        $res = Db::table('chat_group')->find(['g_id'=>$result]);
        $re = [
            'type'   => 'group',
            'avatar' => $res['g_photo'],
            'groupname' => $res['g_name'],
            'id' => $res['g_id']
        ];
        return ['code'=>0,'msg'=>'创建成功','data'=>$re];
    }

    //获取群成员
    public function getMembers()
    {

        $data['g_id'] = input('id');
        if(!$data['g_id']){
            return ['code'=>1,'msg'=>'参数错误，请稍后重新操作'];
        }
        $result = Db::table('chat_group')->find($data);
        if($result['g_group_id']){
            $res = Db::table('chat_user')->where('uid','in',$result['g_group_id'])->select();
        }
        if(isset($res)){
            foreach ($res as $key => $value){
                $data['list'][$key]['username'] = $value['u_nick'];
                $data['list'][$key]['id'] = $value['uid'];
                $data['list'][$key]['avatar'] = $value['u_photo'];
                $data['list'][$key]['sign'] = $value['u_sign'];
            }
            return['code' => 0,'msg' => '','data'=>$data] ;
        }

    }

    //更新心情签名
    public function updateSign()
    {
        $sign = input('sign') ? input('sign') : '';
        if(!$sign){
            return ['code'=>1,'msg'=>'参数传递错误','data'=>5];
        }

        $result = Db::table('chat_user')->where('uid',session('chat_user.uid'))->update(['u_sign'=>$sign]);
        if(!$result){
            return ['code'=>1,'msg'=>'更新失败，请稍后重试','data'=>5];
        }
        return ['code'=>1,'msg'=>'更新成功','data'=>6];
    }



}
