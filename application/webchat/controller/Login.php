<?php

namespace app\webchat\controller;

use think\Db;
class Login
{
    //登录
    public function login()
    {
        if(request()->isPost()){
            $data['u_nick'] = input('nick');
            $data['password'] = input('password');
            if(!$data['u_nick'] || !$data['password']){
                return ['code'=>0, 'msg'=>'用户名和密码必填'];
            }
            //查询数据库
            $loginInfo = Db::table('chat_user')
                ->where('u_nick',$data['u_nick'])
                ->where('password',$data['password'])
                ->find();

            if($loginInfo){
                session('chat_user',$loginInfo);
                return ['code'=>1, 'msg'=>'登录成功！'];
            }else{
                return ['code'=>0, 'msg'=>'用户名或密码错误'];
            }


        }
        return view('login');
    }

    //注册
    public function register()
    {
        if(request()->isPost()){
            $data['u_nick'] = input('nick');
            $data['password'] = input('password');
            $data['u_created'] = date('Y-m-d H:i:s',time());
            if($data['u_nick'] && $data['password']){
                $res = Db::table('chat_user')->insertGetId($data);
                if(!$res){
                    return ['code'=>0, 'msg'=>'注册失败，请稍后重试！'];
                }
                $loginInfo = Db::table('chat_user')->find(['uid'=>$res]);
                session('chat_user',$loginInfo);
                //添加默认分组
                $res = Db::table('chat_friend')->insert(['f_uid'=>$loginInfo['uid'],'f_type_name'=>'我的好友']);

                return ['code'=>1, 'msg'=>'注册成功！'];
            }else{
                return ['code'=>0, 'msg'=>'用户名和密码必填！'];
            }
        }
    }
}