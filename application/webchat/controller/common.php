<?php
namespace app\webchat\controller;

use think\Controller;

class Commonbase extends Controller
{
    public function __construct()
    {
        if(!session('chat_user')){
            return redirect('/chat/login');
        }
    }
}