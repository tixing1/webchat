<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

Route::get('think', function () {
    return 'hello,ThinkPHP5!';
});

Route::any('login', 'webchat/login/login');
Route::post('register', 'webchat/login/register');

Route::any('index', 'webchat/index/index');
Route::get('getinit', 'webchat/index/getInit');
Route::get('find', 'webchat/index/findFriend');
Route::post('addfriend', 'webchat/index/addFriendGroup');
Route::get('msgbox', 'webchat/index/msgBox');
Route::post('agree', 'webchat/index/agreeFriend');
Route::post('refuse', 'webchat/index/refuseFriend');
Route::post('cgroup', 'webchat/index/createGroup');
Route::get('getmembers', 'webchat/index/getMembers');
Route::post('sign', 'webchat/index/updateSign');

return [

];
